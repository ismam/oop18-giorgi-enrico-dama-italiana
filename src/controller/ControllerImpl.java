package controller;

import view.CheckerBoardImpl;
import view.Pair;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import model.CheckerBoardShadow.PiecesType;
import model.CheckerBoardShadow.PlayerName;
import model.CheckerBoardShadowImpl;


/**
 * implementation of {@link Controller} interface.
 */
public class ControllerImpl implements Controller
{
    /**
     *  {@link DIMENSION} dimension of the checkerBoard
     */
    public static final int DIMENSION=8;
    private int turn;
    private CheckerBoardImpl checkerBoard;
    private CheckerBoardShadowImpl checkerBoardShadow;
    List<Pair<Integer,Integer>> piecesIcanEatList = new ArrayList<>();
  
    public void startGame()
    {
        this.checkerBoard = new CheckerBoardImpl(8,8, this);//this is the controller
        this.checkerBoardShadow = new CheckerBoardShadowImpl(this);
        updateCheckerBoard();

    }

    public void startNewGame()
    {
        if (this.checkerBoard!=null)
        {
            this.checkerBoard.close();

        }
        startGame();
    }


    public void click(Pair<Integer,Integer> ev)  
    {
        int x=ev.getX();
        int y=ev.getY();

        PiecesType t = this.checkerBoardShadow.getType(ev.getX(), ev.getY());
        this.turn= this.checkerBoardShadow.getPlayerTurn(t);

        if(turn!=-1) 
        {


            if (t!=PiecesType.E)
            {
                this.checkerBoardShadow.selectItem(x, y, this.checkerBoardShadow.redPathCoordinates(this.availableRedChoices(t, ev)));

            }else 
            {

                if (this.checkerBoardShadow.isSelected())//second click logic
                {
                    this.checkerBoardShadow.movePiece(x, y );
                    this.checkerBoardShadow.unselectItem();//unselect the piece 
                    this.checkerBoard.resetRedBorder(); 

                }else 
                {
                    //	JOptionPane.showMessageDialog(null, "Select a piece");
                }

            }
        }else 
        {
            JOptionPane.showMessageDialog(null, "White Player first");
        }

        updateCheckerBoard();
    }



    public boolean canIEatAgain( int x, int y, PlayerName color) 	
    {
        Boolean eatAgain=false;
        // i am a BlackDama and it's playerBlack turn
        if (color==PlayerName.playerBlack && this.checkerBoardShadow.getType(x, y)==PiecesType.BD) 

        { //if i'm not in the rows and column that give me a null pointer exception for checking x-2, y-2
            //check if i can eat on top left diagonal	
            if((y!=0 && y!=1 && x!=0  && x!=1) && (this.checkerBoardShadow.getType(x-1, y-1)==PiecesType.WP
                    || this.checkerBoardShadow.getType(x-1, y-1)==PiecesType.WD) && (this.checkerBoardShadow.getType(x-2, y-2)==PiecesType.E))
            {
                this.piecesIcanEatList .add(new Pair<Integer, Integer>(x-2, y-2));
                eatAgain=true;
            }
            //check if i can eat on top right diagonal	
            if((y!=7 && y!=6 && x!=0  && x!=1) && (this.checkerBoardShadow.getType(x-1, y+1)==PiecesType.WP
                    || this.checkerBoardShadow.getType(x-1, y+1)==PiecesType.WD) && (this.checkerBoardShadow.getType(x-2, y+2)==PiecesType.E))
            {

                this.piecesIcanEatList .add(new Pair<Integer, Integer>(x-2, y+2));
                eatAgain=true;

            }
            //check if i can eat on bottom left diagonal	
            if((y!=0 && y!=1 && x!=6 && x!=7) && (this.checkerBoardShadow.getType(x+1, y-1)==PiecesType.WP 
                    || this.checkerBoardShadow.getType(x+1, y-1)==PiecesType.WD) && (this.checkerBoardShadow.getType(x+2, y-2)==PiecesType.E))
            {
                this.piecesIcanEatList .add(new Pair<Integer, Integer>(x+2, y-2));
                eatAgain=true;
            }
            //check if i can eat on bottom right diagonal	
            if((y!=7 && y!=6 && x!=6 && x!=7) && (this.checkerBoardShadow.getType(x+1, y+1)==PiecesType.WP 
                    || this.checkerBoardShadow.getType(x+1, y+1)==PiecesType.WD)&& (this.checkerBoardShadow.getType(x+2, y+2)==PiecesType.E))
            {
                this.piecesIcanEatList .add(new Pair<Integer, Integer>(x+2, y+2));
                eatAgain=true;

            }

        } else if(color==PlayerName.playerWhite && this.checkerBoardShadow.getType(x, y)==PiecesType.WD) 
        {
            //if i am a White Dama and it's my turn
            ////check if i can eat on top left diagonal	
            if((y!=0 && y!=1 && x!=0  && x!=1) && (this.checkerBoardShadow.getType(x-1, y-1)==PiecesType.BP
                    || this.checkerBoardShadow.getType(x-1, y-1)==PiecesType.BD) && (this.checkerBoardShadow.getType(x-2, y-2)==PiecesType.E))
            {
                this.piecesIcanEatList .add(new Pair<Integer, Integer>(x-2, y-2));
                eatAgain=true;
            }
            //check if i can eat on top right diagonal	
            if((y!=7 && y!=6 && x!=0  && x!=1) && (this.checkerBoardShadow.getType(x-1, y+1)==PiecesType.BP
                    || this.checkerBoardShadow.getType(x-1, y+1)==PiecesType.BD) && (this.checkerBoardShadow.getType(x-2, y+2)==PiecesType.E))
            {

                this.piecesIcanEatList .add(new Pair<Integer, Integer>(x-2, y+2));
                eatAgain=true;
            }
            //check if i can eat on bottom left diagonal	
            if((y!=0 && y!=1 && x!=6 && x!=7) && (this.checkerBoardShadow.getType(x+1, y-1)==PiecesType.BP 
                    || this.checkerBoardShadow.getType(x+1, y-1)==PiecesType.BD) && (this.checkerBoardShadow.getType(x+2, y-2)==PiecesType.E))
            {
                this.piecesIcanEatList .add(new Pair<Integer, Integer>(x+2, y-2));
                eatAgain=true;
            }
            //check if i can eat on bottom right diagonal	
            if((y!=7 && y!=6 && x!=6 && x!=7) && (this.checkerBoardShadow.getType(x+1, y+1)==PiecesType.BP 
                    || this.checkerBoardShadow.getType(x+1, y+1)==PiecesType.BD)&& (this.checkerBoardShadow.getType(x+2, y+2)==PiecesType.E))
            {
                this.piecesIcanEatList .add(new Pair<Integer, Integer>(x+2, y+2));
                eatAgain=true;
            }
        }

        //2nd part normal pieces that aren't Dama
        else {
            //if i am a black piece and it's playerBlack turn
            if ((this.checkerBoardShadow.getType(x, y)==PiecesType.BP) && color==PlayerName.playerBlack)
            {//check if i can eat on bottom left diagonal	
                if((y!=0 && y!=1 && x!=6 && x!=7) && (this.checkerBoardShadow.getType(x+1, y-1)==PiecesType.WP 
                        || this.checkerBoardShadow.getType(x+1, y-1)==PiecesType.WD) && (this.checkerBoardShadow.getType(x+2, y-2)==PiecesType.E))
                {
                    this.piecesIcanEatList .add(new Pair<Integer, Integer>(x+2, y-2));
                    eatAgain=true;

                }
            }//check if i can eat on bottom right diagonal	
            if ((this.checkerBoardShadow.getType(x, y)==PiecesType.BP) && color==PlayerName.playerBlack)
            {
                if((y!=7 && y!=6 && x!=6 && x!=7) && (this.checkerBoardShadow.getType(x+1, y+1)==PiecesType.WP 
                        || this.checkerBoardShadow.getType(x+1, y+1)==PiecesType.WD)&& (this.checkerBoardShadow.getType(x+2, y+2)==PiecesType.E))
                {
                    this.piecesIcanEatList .add(new Pair<Integer, Integer>(x+2, y+2));
                    eatAgain=true;
                }
            }
            //if i am a White piece and it's playerWhite turn
            //check if i can eat on top left diagonal	
            if((this.checkerBoardShadow.getType(x, y)==PiecesType.WP) && color==PlayerName.playerWhite)
            {
                if((y!=0 && y!=1 && x!=0  && x!=1) && (this.checkerBoardShadow.getType(x-1, y-1)==PiecesType.BP
                        || this.checkerBoardShadow.getType(x-1, y-1)==PiecesType.BD) && (this.checkerBoardShadow.getType(x-2, y-2)==PiecesType.E))
                {
                    this.piecesIcanEatList .add(new Pair<Integer, Integer>(x-2, y-2));
                    eatAgain=true;
                }
            }
            //check if i can eat on top right diagonal	
            if((this.checkerBoardShadow.getType(x, y)==PiecesType.WP) && color==PlayerName.playerWhite)
            {
                if((y!=7 && y!=6 && x!=0  && x!=1) && (this.checkerBoardShadow.getType(x-1, y+1)==PiecesType.BP
                        || this.checkerBoardShadow.getType(x-1, y+1)==PiecesType.BD) && (this.checkerBoardShadow.getType(x-2, y+2)==PiecesType.E))
                {
                    this.piecesIcanEatList .add(new Pair<Integer, Integer>(x-2, y+2));
                    eatAgain=true;
                }
            }


        }

        return eatAgain;

    }

    public List<Pair<Integer,Integer>> getPiecesPriorityToEat(PlayerName color)
    {
        List<Pair<Integer,Integer>> roster = new ArrayList<Pair<Integer,Integer>>();
        for (int x=0; x<CheckerBoardShadowImpl.DIMENSION; x++)
        {
            for (int y=0;y<CheckerBoardShadowImpl.DIMENSION; y++)

                //if i am a black Dama and it player Black turn
                if (color==PlayerName.playerBlack && this.checkerBoardShadow.getType(x, y)==PiecesType.BD) 

                {
                    //check if i can eat on top left diagonal	
                    if((y!=0 && y!=1 && x!=0  && x!=1) && (this.checkerBoardShadow.getType(x-1, y-1)==PiecesType.WP
                            || this.checkerBoardShadow.getType(x-1, y-1)==PiecesType.WD) && (this.checkerBoardShadow.getType(x-2, y-2)==PiecesType.E))
                    {
                        roster.add(new Pair<Integer, Integer>(x, y));
                    }
                    //check if i can eat on top right diagonal
                    if((y!=7 && y!=6 && x!=0  && x!=1) && (this.checkerBoardShadow.getType(x-1, y+1)==PiecesType.WP
                            || this.checkerBoardShadow.getType(x-1, y+1)==PiecesType.WD) && (this.checkerBoardShadow.getType(x-2, y+2)==PiecesType.E))
                    {

                        roster.add(new Pair<Integer, Integer>(x, y));

                    }
                    //check if i can eat on bottom left diagonal
                    if((y!=0 && y!=1 && x!=6 && x!=7) && (this.checkerBoardShadow.getType(x+1, y-1)==PiecesType.WP 
                            || this.checkerBoardShadow.getType(x+1, y-1)==PiecesType.WD) && (this.checkerBoardShadow.getType(x+2, y-2)==PiecesType.E))
                    {
                        roster.add(new Pair<Integer, Integer>(x, y));

                    }
                    //check if i can eat on bottom right diagonal
                    if((y!=7 && y!=6 && x!=6 && x!=7) && (this.checkerBoardShadow.getType(x+1, y+1)==PiecesType.WP 
                            || this.checkerBoardShadow.getType(x+1, y+1)==PiecesType.WD)&& (this.checkerBoardShadow.getType(x+2, y+2)==PiecesType.E))
                    {
                        roster.add(new Pair<Integer, Integer>(x, y));

                    }


                } else if(color==PlayerName.playerWhite && this.checkerBoardShadow.getType(x, y)==PiecesType.WD) 
                {
                    //if i am a whiteDama and it's player White turn
                    //check if i can eat on top left diagonal
                    if((y!=0 && y!=1 && x!=0  && x!=1) && (this.checkerBoardShadow.getType(x-1, y-1)==PiecesType.BP
                            || this.checkerBoardShadow.getType(x-1, y-1)==PiecesType.BD) && (this.checkerBoardShadow.getType(x-2, y-2)==PiecesType.E))
                    {
                        roster.add(new Pair<Integer, Integer>(x, y));
                    }
                    //check if i can eat on top right diagonal
                    if((y!=7 && y!=6 && x!=0  && x!=1) && (this.checkerBoardShadow.getType(x-1, y+1)==PiecesType.BP
                            || this.checkerBoardShadow.getType(x-1, y+1)==PiecesType.BD) && (this.checkerBoardShadow.getType(x-2, y+2)==PiecesType.E))
                    {

                        roster.add(new Pair<Integer, Integer>(x, y));

                    }
                    //check if i can eat on bottom left diagonal
                    if((y!=0 && y!=1 && x!=6 && x!=7) && (this.checkerBoardShadow.getType(x+1, y-1)==PiecesType.BP 
                            || this.checkerBoardShadow.getType(x+1, y-1)==PiecesType.BD) && (this.checkerBoardShadow.getType(x+2, y-2)==PiecesType.E))
                    {
                        roster.add(new Pair<Integer, Integer>(x, y));

                    }
                    //check if i can eat on bottom right diagonal
                    if((y!=7 && y!=6 && x!=6 && x!=7) && (this.checkerBoardShadow.getType(x+1, y+1)==PiecesType.BP 
                            || this.checkerBoardShadow.getType(x+1, y+1)==PiecesType.BD)&& (this.checkerBoardShadow.getType(x+2, y+2)==PiecesType.E))
                    {
                        roster.add(new Pair<Integer, Integer>(x, y));

                    }
                }

            //2nd part normal pieces that aren't Dama
                else {

                    if ((this.checkerBoardShadow.getType(x, y)==PiecesType.BP) && color==PlayerName.playerBlack)
                    {
                        //check if i can eat on bottom left diagonal
                        if((y!=0 && y!=1 && x!=6 && x!=7) && (this.checkerBoardShadow.getType(x+1, y-1)==PiecesType.WP 
                                || this.checkerBoardShadow.getType(x+1, y-1)==PiecesType.WD) && (this.checkerBoardShadow.getType(x+2, y-2)==PiecesType.E))
                        {
                            roster.add(new Pair<Integer, Integer>(x, y));

                        }


                        //check if i can eat on bottom right diagonal
                        if((y!=7 && y!=6 && x!=6 && x!=7) && (this.checkerBoardShadow.getType(x+1, y+1)==PiecesType.WP 
                                || this.checkerBoardShadow.getType(x+1, y+1)==PiecesType.WD)&& (this.checkerBoardShadow.getType(x+2, y+2)==PiecesType.E))
                        {
                            roster.add(new Pair<Integer, Integer>(x, y));

                        }
                    }
                    if((this.checkerBoardShadow.getType(x, y)==PiecesType.WP) && color==PlayerName.playerWhite)
                    {
                        //check if i can eat on top left diagonal
                        if((y!=0 && y!=1 && x!=0  && x!=1) && (this.checkerBoardShadow.getType(x-1, y-1)==PiecesType.BP
                                || this.checkerBoardShadow.getType(x-1, y-1)==PiecesType.BD) && (this.checkerBoardShadow.getType(x-2, y-2)==PiecesType.E))
                        {

                            roster.add(new Pair<Integer, Integer>(x, y));

                        }


                        //check if i can eat on top right diagonal
                        if((y!=7 && y!=6 && x!=0  && x!=1) && (this.checkerBoardShadow.getType(x-1, y+1)==PiecesType.BP
                                || this.checkerBoardShadow.getType(x-1, y+1)==PiecesType.BD) && (this.checkerBoardShadow.getType(x-2, y+2)==PiecesType.E))
                        {

                            roster.add(new Pair<Integer, Integer>(x, y));

                        }
                    }

                }

        }
        return roster;

    }


    public List<Pair<Integer,Integer>> availableRedChoices(PiecesType t, Pair<Integer,Integer> ev)  {

        int flagBlack=-1;
        int flagWhite=-1;
        this.checkerBoard.resetRedBorder(); 
        int x=ev.getX();
        int y=ev.getY();
        List<Pair<Integer,Integer>> p= new ArrayList<>();

        if (this.checkerBoardShadow.getMustEatPiece()!=null && this.checkerBoardShadow.getMustEatPiece()!=ev)
        {

            //JOptionPane.showMessageDialog(null, "You can eat again");
            p.addAll(piecesIcanEatList);
        }
        if (!getPiecesPriorityToEat(this.checkerBoardShadow.getPlayerColor()).isEmpty() && 
                !getPiecesPriorityToEat(this.checkerBoardShadow.getPlayerColor()).contains(new Pair<Integer, Integer>(x, y)))
        {
            JOptionPane.showMessageDialog(null, "You had to eat");
            return p;
        }
        //if the selected piece is a  Dama  i invoke the DamaRoute method, that implements movements for DamaPieces
        if(t==PiecesType.WD || t==PiecesType.BD) {
            p.addAll(damaRoute(t,ev));
        } else {

            //if the selected piece is a normal black piece
            if(this.checkerBoardShadow.getPlayerColor()==PlayerName.playerBlack) 
            {	
                if(t==PiecesType.BP) {
                    //if the black piece is on a row that able it to upgrade to Dama, i invoke the CreateDama method
                    if(ev.getX()==6 || ev.getX()==5) 
                    {
                        p.addAll(this.createDama( t, ev));

                    } else {//black piece can eat on bottom left diagonal
                        if((ev.getY()!=0 && ev.getY()!=1 && ev.getX()!=6 && ev.getX()!=7 ) && (this.checkerBoardShadow.getType(x+1, y-1)==PiecesType.WP 
                                || this.checkerBoardShadow.getType(x+1, y-1)==PiecesType.WD) && (this.checkerBoardShadow.getType(x+2, y-2)==PiecesType.E))
                        {
                            this.checkerBoard.setBorder(ev.getX()+2, ev.getY()-2);
                            this.checkerBoardShadow.getEatPiece(x+1,y-1);
                            flagBlack++;
                            p.add(new Pair<>(x+2,y-2));

                        }
                        //black piece can eat on bottom right diagonal
                        if((ev.getY()!=7 && ev.getY()!=6 && ev.getX()!=6 && ev.getX()!=7 ) && (this.checkerBoardShadow.getType(x+1, y+1)==PiecesType.WP 
                                || this.checkerBoardShadow.getType(x+1, y+1)==PiecesType.WD)&& (this.checkerBoardShadow.getType(x+2, y+2)==PiecesType.E))
                        {
                            this.checkerBoard.setBorder(ev.getX()+2, ev.getY()+2);
                            this.checkerBoardShadow.getEatPiece(x+1,y+1);
                            p.add(new Pair<>(x+2,y+2));
                            flagBlack++;

                        }
                        if(flagBlack==-1) {
                            if(ev.getY()!=0 && ev.getX()!=7 && ev.getX()!=6 ) 
                            { //Black piece can only move one step without eating, on Bottom left diagonal
                                if (this.checkerBoardShadow.getType(x+1, y-1)==PiecesType.E) 
                                {
                                    this.checkerBoard.setBorder(ev.getX()+1, ev.getY()-1);
                                    this.checkerBoardShadow.getEatPiece(-1,-1);
                                    p.add(new Pair<>(x+1,y-1)); 
                                }
                            }
                            if(ev.getY()!=7  && ev.getX()!=7 &&  ev.getX()!=6 ) 
                            {  //Black piece can only move one step without eating, on Bottom right diagonal
                                if(this.checkerBoardShadow.getType(x+1, y+1)==PiecesType.E ) 
                                {
                                    this.checkerBoard.setBorder(ev.getX()+1, ev.getY()+1);
                                    this.checkerBoardShadow.getEatPiece(-1,-1);
                                    p.add(new Pair<>(x+1,y+1));
                                }
                            }
                        }
                    }
                }
            } 	//white pieces
            else if(t==PiecesType.WP) 
            {

                if(this.checkerBoardShadow.getPlayerColor()==PlayerName.playerWhite) 
                {	
                    //if the white piece is on a row that able it to upgrade to Dama, i invoke the CreateDama method
                    if(ev.getX()==1 || ev.getX()==2) 
                    {
                        p.addAll(this.createDama( t, ev));
                    } 
                    else {
                        //white piece can eat on top left diagonal
                        if((ev.getY()!=0 && ev.getY()!=1 && ev.getX()!=0  && ev.getX()!=1 && ev.getX()!=2) &&
                                (this.checkerBoardShadow.getType(x-1, y-1)==PiecesType.BP 
                                || this.checkerBoardShadow.getType(x-1, y-1)==PiecesType.BD) && (this.checkerBoardShadow.getType(x-2, y-2)==PiecesType.E))
                        {

                            this.checkerBoard.setBorder(ev.getX()-2, ev.getY()-2);
                            this.checkerBoardShadow.getEatPiece(x-1,y-1);
                            p.add(new Pair<>(x-2,y-2));
                            flagWhite++;


                        }
                        //white piece can eat on top right diagonal
                        if((ev.getY()!=7 && ev.getY()!=6 && ev.getX()!=0  && ev.getX()!=1 && ev.getX()!=2) && 
                                (this.checkerBoardShadow.getType(x-1, y+1)==PiecesType.BP
                                || this.checkerBoardShadow.getType(x-1, y+1)==PiecesType.BD) && (this.checkerBoardShadow.getType(x-2, y+2)==PiecesType.E))
                        {

                            this.checkerBoard.setBorder(ev.getX()-2, ev.getY()+2);
                            this.checkerBoardShadow.getEatPiece(x-1,y+1);
                            p.add(new Pair<>(x-2,y+2));
                            flagWhite++;

                        } 
                        //if i don't eat, i can only move one step diagonal let's check it!
                        if (flagWhite==-1)
                        {	 
                            if(ev.getY()!=0 && ev.getX()!=0 && ev.getX()!=1 )
                            { //top left
                                if (this.checkerBoardShadow.getType(x-1, y-1)==PiecesType.E) 
                                {

                                    this.checkerBoard.setBorder(ev.getX()-1, ev.getY()-1);
                                    this.checkerBoardShadow.getEatPiece(-1,-1);
                                    p.add(new Pair<>(x-1,y-1)); 

                                }
                            }
                            if(ev.getY()!=7 && ev.getX()!=0 &&ev.getX()!=1 ) 
                            {  //top Right
                                if(this.checkerBoardShadow.getType(x-1, y+1)==PiecesType.E)
                                {

                                    this.checkerBoard.setBorder(ev.getX()-1, ev.getY()+1);
                                    this.checkerBoardShadow.getEatPiece(-1,-1);
                                    p.add(new Pair<>(x-1,y+1));

                                }
                            }
                        }
                    }
                }
            }
        }

        return p;

    }


    public List<Pair<Integer,Integer>> damaRoute(PiecesType t, Pair<Integer, Integer> ev)
    {
        int flagEatBlack=-1;
        this.checkerBoard.resetRedBorder(); 
        int x=ev.getX();
        int y=ev.getY();
        List<Pair<Integer,Integer>> p= new ArrayList<>();
        //if i can eat again with a Dama
        if (!getPiecesPriorityToEat(this.checkerBoardShadow.getPlayerColor()).isEmpty() && 
                !getPiecesPriorityToEat(this.checkerBoardShadow.getPlayerColor()).contains(new Pair<Integer, Integer>(x, y)))
        {
            //JOptionPane.showMessageDialog(null, "You had to eat");
            return p;
        }
        if(this.checkerBoardShadow.getPlayerColor()==PlayerName.playerBlack) 
        {	
            //if i am a black Dama let's check the diagonal where i can eat
            if(t==PiecesType.BD) 
            {	
                //top left
                if((ev.getY()!=0 && ev.getY()!=1 && ev.getX()!=0  && ev.getX()!=1)
                        && (this.checkerBoardShadow.getType(x-1, y-1)==PiecesType.WP || this.checkerBoardShadow.getType(x-1, y-1)==PiecesType.WD) 
                        && (this.checkerBoardShadow.getType(x-2, y-2)==PiecesType.E))
                {
                    this.checkerBoard.setBorder(ev.getX()-2, ev.getY()-2);
                    this.checkerBoardShadow.getEatPiece(x-1,y-1);
                    p.add(new Pair<>(x-2,y-2));
                    this.checkerBoardShadow.setDama(x-2, y-2);
                    flagEatBlack++;
                }
                //top right
                if((ev.getY()!=7 && ev.getY()!=6 && ev.getX()!=0  && ev.getX()!=1) && (this.checkerBoardShadow.getType(x-1, y+1)==PiecesType.WP
                        || this.checkerBoardShadow.getType(x-1, y+1)==PiecesType.WD) && (this.checkerBoardShadow.getType(x-2, y+2)==PiecesType.E))
                {
                    this.checkerBoard.setBorder(ev.getX()-2, ev.getY()+2);
                    this.checkerBoardShadow.getEatPiece(x-1,y+1);
                    p.add(new Pair<>(x-2,y+2));
                    this.checkerBoardShadow.setDama(x-2, y+2);
                    flagEatBlack++;
                } 
                //bottom left
                if((ev.getY()!=0 && ev.getY()!=1 && ev.getX()!=6 && ev.getX()!=7) && (this.checkerBoardShadow.getType(x+1, y-1)==PiecesType.WP 
                        || this.checkerBoardShadow.getType(x+1, y-1)==PiecesType.WD) && (this.checkerBoardShadow.getType(x+2, y-2)==PiecesType.E))
                {
                    this.checkerBoard.setBorder(ev.getX()+2, ev.getY()-2);
                    this.checkerBoardShadow.getEatPiece(x+1,y-1);
                    p.add(new Pair<>(x+2,y-2));
                    this.checkerBoardShadow.setDama(x+2, y-2);
                    flagEatBlack++;

                }
                //bottom right
                if((ev.getY()!=7 && ev.getY()!=6 && ev.getX()!=6 && ev.getX()!=7) && (this.checkerBoardShadow.getType(x+1, y+1)==PiecesType.WP 
                        || this.checkerBoardShadow.getType(x+1, y+1)==PiecesType.WD)&& (this.checkerBoardShadow.getType(x+2, y+2)==PiecesType.E))
                {
                    this.checkerBoard.setBorder(ev.getX()+2, ev.getY()+2);
                    this.checkerBoardShadow.getEatPiece(x+1,y+1);
                    p.add(new Pair<>(x+2,y+2));
                    flagEatBlack++;
                    this.checkerBoardShadow.setDama(x+2, y+2);

                } 
                //if i am a Dama but i can't eat, only move
                if(flagEatBlack==-1) 
                {   
                    //top left diagonal
                    if(ev.getY()!=0 && ev.getX()!=0 )
                    { 
                        if (this.checkerBoardShadow.getType(x-1, y-1)==PiecesType.E) 
                        {
                            this.checkerBoard.setBorder(ev.getX()-1, ev.getY()-1);
                            p.add(new Pair<>(x-1,y-1));
                            this.checkerBoardShadow.setDama(x-1, y-1);

                        }
                    }
                    //top right diagonal
                    if(ev.getY()!=7 && ev.getX()!=0) 
                    {  
                        if(this.checkerBoardShadow.getType(x-1, y+1)==PiecesType.E)
                        {
                            this.checkerBoard.setBorder(ev.getX()-1, ev.getY()+1);
                            p.add(new Pair<>(x-1,y+1));
                            this.checkerBoardShadow.setDama(x-2, y+2);

                        }
                    }
                    //bottom left diagonal
                    if(ev.getY()!=0 && ev.getX()!=7) 
                    { 
                        if (this.checkerBoardShadow.getType(x+1, y-1)==PiecesType.E) 
                        {

                            this.checkerBoard.setBorder(ev.getX()+1, ev.getY()-1);
                            p.add(new Pair<>(x+1,y-1));
                            this.checkerBoardShadow.setDama(x+1, y-1);
                        }
                    }
                    //bottom right diagonal
                    if(ev.getY()!=7  && ev.getX()!=7) 
                    { 
                        if(this.checkerBoardShadow.getType(x+1, y+1)==PiecesType.E ) 
                        { 
                            this.checkerBoard.setBorder(ev.getX()+1, ev.getY()+1);
                            p.add(new Pair<>(x+1,y+1));
                            this.checkerBoardShadow.setDama(+1, y+1);
                        }
                    }



                }
            }
        }
        //if i am a white Dama and it's playerWhite turn
        if(this.checkerBoardShadow.getPlayerColor()==PlayerName.playerWhite) 
        {	
            if (t==PiecesType.WD) 
            {
                //Top left diagonal
                if((ev.getY()!=0 && ev.getY()!=1 && ev.getX()!=0  && ev.getX()!=1)
                        && (this.checkerBoardShadow.getType(x-1, y-1)==PiecesType.BP || this.checkerBoardShadow.getType(x-1, y-1)==PiecesType.BD) 
                        && (this.checkerBoardShadow.getType(x-2, y-2)==PiecesType.E))
                {
                    this.checkerBoard.setBorder(ev.getX()-2, ev.getY()-2);
                    this.checkerBoardShadow.getEatPiece(x-1,y-1);
                    p.add(new Pair<>(x-2,y-2));
                    this.checkerBoardShadow.setDama(x-2, y-2);
                    flagEatBlack++;

                }
                //top right diagonal
                if((ev.getY()!=7 && ev.getY()!=6 && ev.getX()!=0  && ev.getX()!=1) && (this.checkerBoardShadow.getType(x-1, y+1)==PiecesType.BP
                        || this.checkerBoardShadow.getType(x-1, y+1)==PiecesType.BD) && (this.checkerBoardShadow.getType(x-2, y+2)==PiecesType.E))
                {
                    this.checkerBoard.setBorder(ev.getX()-2, ev.getY()+2);
                    this.checkerBoardShadow.getEatPiece(x-1,y+1);
                    p.add(new Pair<>(x-2,y+2));
                    this.checkerBoardShadow.setDama(x-2, y+2);
                    flagEatBlack++;

                } 
                //bottom left diagonal
                if((ev.getY()!=0 && ev.getY()!=1 && ev.getX()!=6 && ev.getX()!=7) && (this.checkerBoardShadow.getType(x+1, y-1)==PiecesType.BP 
                        || this.checkerBoardShadow.getType(x+1, y-1)==PiecesType.BD) && (this.checkerBoardShadow.getType(x+2, y-2)==PiecesType.E))
                {
                    this.checkerBoard.setBorder(ev.getX()+2, ev.getY()-2);
                    this.checkerBoardShadow.getEatPiece(x+1,y-1);
                    p.add(new Pair<>(x+2,y-2));
                    this.checkerBoardShadow.setDama(x+2, y-2);
                    flagEatBlack++;

                } 
                //bottom right diagonal
                if((ev.getY()!=7 && ev.getY()!=6 && ev.getX()!=6 && ev.getX()!=7) && (this.checkerBoardShadow.getType(x+1, y+1)==PiecesType.BP 
                        || this.checkerBoardShadow.getType(x+1, y+1)==PiecesType.BD)&& (this.checkerBoardShadow.getType(x+2, y+2)==PiecesType.E))
                {
                    this.checkerBoard.setBorder(ev.getX()+2, ev.getY()+2);
                    this.checkerBoardShadow.getEatPiece(x+1,y+1);
                    p.add(new Pair<>(x+2,y+2));
                    this.checkerBoardShadow.setDama(x+2, y+2);
                    flagEatBlack++;
                }
                //if i can't eat but only move one step on diagonal 
                if(flagEatBlack==-1) 
                {
                    //top left diagonal
                    if(ev.getY()!=0 && ev.getX()!=0 )
                    { 
                        if (this.checkerBoardShadow.getType(x-1, y-1)==PiecesType.E) 
                        {
                            this.checkerBoard.setBorder(ev.getX()-1, ev.getY()-1);
                            p.add(new Pair<>(x-1,y-1));
                            this.checkerBoardShadow.setDama(x-1, y-1);

                        }
                    }
                    //top right diagonal
                    if(ev.getY()!=7 && ev.getX()!=0) 
                    {  
                        if(this.checkerBoardShadow.getType(x-1, y+1)==PiecesType.E)
                        {
                            this.checkerBoard.setBorder(ev.getX()-1, ev.getY()+1);
                            p.add(new Pair<>(x-1,y+1));
                            this.checkerBoardShadow.setDama(x-1, y+1);

                        }
                    }
                    //bottom left diagonal
                    if(ev.getY()!=0 && ev.getX()!=7) 
                    { 
                        if (this.checkerBoardShadow.getType(x+1, y-1)==PiecesType.E) 
                        {

                            this.checkerBoard.setBorder(ev.getX()+1, ev.getY()-1);
                            p.add(new Pair<>(x+1,y-1)); 
                            this.checkerBoardShadow.setDama(x+1, y-1);
                        }
                    }
                    //bottom right diagonal
                    if(ev.getY()!=7  && ev.getX()!=7) 
                    { 
                        if(this.checkerBoardShadow.getType(x+1, y+1)==PiecesType.E ) 
                        { 
                            this.checkerBoard.setBorder(ev.getX()+1, ev.getY()+1);
                            p.add(new Pair<>(x+1,y+1));
                            this.checkerBoardShadow.setDama(x+1, y+1);
                        }
                    }

                }

            }
        }

        return p;
    }


    public List<Pair<Integer,Integer>> createDama(PiecesType t, Pair<Integer, Integer> ev)
    {
        this.checkerBoard.resetRedBorder(); 
        int x=ev.getX();
        int y=ev.getY();
        List<Pair<Integer,Integer>> p= new ArrayList<>();
        int flag=-1;
        //if i am a black piece let's check if i can become a Dama
        if(t==PiecesType.BP) 
        {
            //bottom left diagonal
            if ((ev.getY()!=0 && ev.getY()!=1  && ev.getX()==5)&&(this.checkerBoardShadow.getType(x+1, y-1)==PiecesType.WP ||
                    (this.checkerBoardShadow.getType(x+1, y-1)==PiecesType.WD)) && this.checkerBoardShadow.getType(x+2, y-2)==PiecesType.E)
            {
                this.checkerBoard.setBorder(ev.getX()+2, ev.getY()-2);
                this.checkerBoardShadow.getEatPiece(x+1,y-1);
                p.add(new Pair<>(x+2,y-2)); 
                flag++;
                this.checkerBoardShadow.setDama(x+2, y-2);

            }
            //bottom right diagonal
            if((ev.getY()!=7 && ev.getY()!=6 && ev.getX()==5)&&((this.checkerBoardShadow.getType(x+1, y+1)==PiecesType.WP)||
                    (this.checkerBoardShadow.getType(x+1, y+1)==PiecesType.WD)) && this.checkerBoardShadow.getType(x+2, y+2)==PiecesType.E)
            {				
                this.checkerBoard.setBorder(ev.getX()+2, ev.getY()+2);
                this.checkerBoardShadow.getEatPiece(x+1,y+1);
                p.add(new Pair<>(x+2,y+2)); 
                flag++;
                this.checkerBoardShadow.setDama(x+2, y+2);
            }
            //if i can't eat, let's check if i can move one step on diagonal
            if(flag==-1)
            {
                //bottom left diagonal
                if ((ev.getY()!=0 && ev.getX()==6 )&&(this.checkerBoardShadow.getType(x+1, y-1)==PiecesType.E))
                {
                    this.checkerBoard.setBorder(ev.getX()+1, ev.getY()-1);
                    this.checkerBoardShadow.getEatPiece(-1,-1);
                    p.add(new Pair<>(x+1,y-1)); 
                    this.checkerBoardShadow.setDama(x+1, y-1);

                }
                //bottom right diagonal
                if((ev.getY()!=7 && ev.getX()==6)&&(this.checkerBoardShadow.getType(x+1, y+1)==PiecesType.E))
                {				
                    this.checkerBoard.setBorder(ev.getX()+1, ev.getY()+1);
                    this.checkerBoardShadow.getEatPiece(-1,-1);
                    p.add(new Pair<>(x+1,y+1)); 
                    this.checkerBoardShadow.setDama(x+1, y+1);
                }
                //x=5 and i can move one step, but i can't set the Dama, so i set Dama on -1 value that means, no Dama
                //bottom left diagonal
                if ((ev.getY()!=0 && ev.getX()==5 )&&(this.checkerBoardShadow.getType(x+1, y-1)==PiecesType.E))
                {
                    this.checkerBoard.setBorder(ev.getX()+1, ev.getY()-1);
                    this.checkerBoardShadow.getEatPiece(-1,-1);
                    p.add(new Pair<>(x+1,y-1)); 
                    this.checkerBoardShadow.setDama(-1, -1);

                }
                //bottom right diagonal
                if((ev.getY()!=7 && ev.getX()==5)&&(this.checkerBoardShadow.getType(x+1, y+1)==PiecesType.E))
                {				
                    this.checkerBoard.setBorder(ev.getX()+1, ev.getY()+1);
                    this.checkerBoardShadow.getEatPiece(-1,-1);
                    p.add(new Pair<>(x+1,y+1)); 
                    this.checkerBoardShadow.setDama(-1, -1);
                }


            }

        }else if(t==PiecesType.WP) 
        {
            //if i am a white piece, let's check if i can become a Dama
            //can i eat on top left Diagonal?
            if ((ev.getY()!=0 && ev.getY()!=1  && ev.getX()==2 )&&(this.checkerBoardShadow.getType(x-1, y-1)==PiecesType.BP
                    ||this.checkerBoardShadow.getType(x-1, y-1)==PiecesType.BD) && this.checkerBoardShadow.getType(x-2, y-2)==PiecesType.E)
            {
                this.checkerBoard.setBorder(ev.getX()-2, ev.getY()-2);
                this.checkerBoardShadow.getEatPiece(x-1,y-1);
                p.add(new Pair<>(x-2,y-2)); 
                flag++;
                this.checkerBoardShadow.setDama(x-2, y-2);

            }
            //can i eat on top right Diagonal?
            if ((ev.getY()!=7 && ev.getY()!=6 && ev.getX()==2)&&(this.checkerBoardShadow.getType(x-1, y+1)==PiecesType.BP
                    ||this.checkerBoardShadow.getType(x-1, y+1)==PiecesType.BD) && this.checkerBoardShadow.getType(x-2, y+2)==PiecesType.E)
            {
                this.checkerBoard.setBorder(ev.getX()-2, ev.getY()+2);
                this.checkerBoardShadow.getEatPiece(x-1,y+1);
                p.add(new Pair<>(x-2,y+2)); 
                flag++;
                this.checkerBoardShadow.setDama(x-2, y+2);

            }
            //can i become a Dama without eating?
            if(flag==-1) 
            {
                //can i move on top left Diagonal?
                if ((ev.getY()!=0 && ev.getX()==1)&&(this.checkerBoardShadow.getType(x-1, y-1)==PiecesType.E))
                {
                    this.checkerBoard.setBorder(ev.getX()-1, ev.getY()-1);
                    this.checkerBoardShadow.getEatPiece(-1,-1);
                    p.add(new Pair<>(x-1,y-1)); 
                    this.checkerBoardShadow.setDama(x-1, y-1);

                }
                //can i move on top right Diagonal?
                if((ev.getY()!=7 && ev.getX()==1)&&(this.checkerBoardShadow.getType(x-1, y+1)==PiecesType.E))
                {				
                    this.checkerBoard.setBorder(ev.getX()-1, ev.getY()+1);
                    this.checkerBoardShadow.getEatPiece(-1,-1);
                    p.add(new Pair<>(x-1,y+1)); 
                    this.checkerBoardShadow.setDama(x-1, y+1);
                }
                //x=2 and i can move one step, but i can't set the Dama, so i set Dama on -1 value that means, no Dama 
                //top left
                if ((ev.getY()!=0 && ev.getX()==2)&&(this.checkerBoardShadow.getType(x-1, y-1)==PiecesType.E))
                {
                    this.checkerBoard.setBorder(ev.getX()-1, ev.getY()-1);
                    this.checkerBoardShadow.getEatPiece(-1,-1);
                    p.add(new Pair<>(x-1,y-1)); 
                    this.checkerBoardShadow.setDama(-1, -1);

                }
                //top right
                if((ev.getY()!=7 && ev.getX()==2)&&(this.checkerBoardShadow.getType(x-1, y+1)==PiecesType.E))
                {				
                    this.checkerBoard.setBorder(ev.getX()-1, ev.getY()+1);
                    this.checkerBoardShadow.getEatPiece(-1,-1);
                    p.add(new Pair<>(x-1,y+1)); 
                    this.checkerBoardShadow.setDama(-1, -1);
                }
            }
        }
        return p;
    }




    public void updateCheckerBoard() 
    {
        for (int r=0; r<DIMENSION; r++)
        {
            for (int c=0; c<DIMENSION; c++)
            {

                try {
					this.checkerBoard.setPieces(r, c, this.checkerBoardShadow.getType(r, c));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

            }
        }
    }


}
